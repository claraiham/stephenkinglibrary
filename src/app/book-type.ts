export type Book = {
    key: string
    title: string
    author_name: string
    description: {
        type: string,
        value: string
    }
    covers: number[]
}