import { Routes } from '@angular/router';
import { BookDetailsComponent } from './book-details/book-details.component';
import { AppComponent } from './app.component';
import { BookListComponent } from './book-list/book-list.component';

export const routes: Routes = [
    {path:'', component: BookListComponent},
    {path: 'book-details/:id', component: BookDetailsComponent}
];
