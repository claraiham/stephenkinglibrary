import { Component, OnInit, inject } from '@angular/core';
import { RouterModule, RouterOutlet } from '@angular/router';
import { BookCardComponent } from '../book-card/book-card.component';
import { BookDetailsComponent } from '../book-details/book-details.component';
import { BookListAPIService } from '../book-list-api.service';
import { Book } from '../book-type';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-book-list',
  standalone: true,
  imports: [RouterOutlet, BookCardComponent, RouterModule, BookDetailsComponent, ReactiveFormsModule],
  templateUrl: './book-list.component.html',
  styleUrl: './book-list.component.css'
})
export class BookListComponent implements OnInit{
  bookList: Book [] = []
  filteredList: Book[]=[]
  bookListAPI: BookListAPIService = inject(BookListAPIService)

  constructor(){
    
  }

  async ngOnInit(){
    this.bookList = await this.bookListAPI.getAllBooks()
    this.filteredList = this.bookList
  }

  filterResults(text: string) {
    if (!text) {
      this.filteredList = this.bookList;
      return;
    }
  
    this.filteredList = this.bookList.filter(
      book => book?.title.toLowerCase().includes(text.toLowerCase())
    );
  }
}
