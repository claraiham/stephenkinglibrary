import { Injectable, inject } from '@angular/core';
import { Book } from './book-type';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BookListAPIService {

  urlBooks = "https://openlibrary.org/authors/OL2162284A/works.json?limits=100"
  bookList: Book [] = []

  // constructor(private selectedBook: Book) {
  //   selectedBook = this.selectedBook
  //  }

  

  async getAllBooks(): Promise<Book[]> {
    const result = await fetch(this.urlBooks)
      .then((response) => response.json())
      .then((response)=> response.entries) // récupérer le tableaux entries qui contient tout
      .then((response: Book [])=> {
        console.log(this.bookList)
        this.bookList = response
         return response
      })
      return result;
  }

  async getBook(url: any): Promise<Book> {
    const result = await fetch(url)
      .then((response) => response.json())
      // .then((response)=> response)
      .then((response: Book)=> {
        console.log(response)
         return response
      })
      return result;
  }

  async getImage(url:any){
    const result = await fetch(url)
      .then((response) => response.json())
      .then((response)=> response)
      .then((response: Book)=> {
        console.log(response)
         return response
      })
      return result;
  }

  displayDescription(bookId: string){
    return this.bookList?.find(book => book.key === bookId)
  }

}
