import { TestBed } from '@angular/core/testing';

import { BookListAPIService } from './book-list-api.service';

describe('BookListAPIService', () => {
  let service: BookListAPIService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BookListAPIService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
