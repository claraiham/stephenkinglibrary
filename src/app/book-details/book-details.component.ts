import { Component, Input, inject } from '@angular/core';
import { Book } from '../book-type';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { BookListAPIService } from '../book-list-api.service';


@Component({
  selector: 'app-book-details',
  standalone: true,
  imports: [RouterModule],
  templateUrl: './book-details.component.html',
  styleUrl: './book-details.component.css'
})
export class BookDetailsComponent {

  route: ActivatedRoute = inject(ActivatedRoute)
  bookListAPI = inject(BookListAPIService)
  // singleBookAPI = inject(SingleBookAPIService)
  bookId = ''
  url = '' 
  book: Book | undefined

  // bookList: Book [] = []

  constructor(){
    this.bookId = this.route.snapshot.params['id']
    this.url = 'https://openlibrary.org'+this.bookId+'.json'
    console.log(this.url)
    // this.book = this.bookListAPI.displayDescription(this.bookId)
  }
  async ngOnInit(){
    // this.bookList = await this.bookListAPI.getAllBooks()
    // this.book = this.bookListAPI.displayDescription(this.bookId)
    this.book = await this.bookListAPI.getBook(this.url)
    
  }

}
